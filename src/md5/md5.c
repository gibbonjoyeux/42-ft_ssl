
#include "ft_ssl.h"

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void		load_blocks(t_md5 *md5)
{
	md5->block_number = md5->text_length / 64;
	ct_putuint64(md5->block_number); ct_putchar('\n');
	if (md5->block_number == md5->text_length / 64)
		(md5->block_number)++;
	ct_putuint64(md5->block_number); ct_putchar('\n');
	if (md5->text_length && md5->text_length % 64 > 55)
		(md5->block_number)++;
	ct_putuint64(md5->block_number); ct_putchar('\n');
	md5->blocks = calloc(md5->block_number, sizeof(t_md5_block));
	if (md5->blocks == NULL)
		ct_exit_fail("Not enough memory to allocate md5 512b blocks.");
	ct_memcpy(md5->blocks, md5->string, md5->text_length);
	md5->blocks[0].t8[md5->text_length] = 0b10000000;
	md5->blocks[md5->block_number - 1].t64[7] = md5->text_length;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

char			*ssl_md5(char *string)
{
	t_md5		md5;

	md5.buffers[0] = MD5_A;
	md5.buffers[1] = MD5_B;
	md5.buffers[2] = MD5_C;
	md5.buffers[3] = MD5_D;
	md5.string = string;
	md5.text_length = ct_strlen(string);
	load_blocks(&md5);
	show_alloc_mem();
	show_alloc_mem_slot(md5.blocks);
	free(md5.blocks);
	return (ct_itoa(md5.text_length));
}
